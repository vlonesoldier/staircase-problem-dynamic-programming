/**
 * @param {number} n
 * @return {number}
 */

let arr = [0, 1, 2];

var climbStairs = function(n) {
    
    if(n === 1)
        return arr[n];

    if(n === 2)
        return arr[n];
    
    if(arr[n])
        return arr[n]
        
    arr[n] = climbStairs(n - 1) + climbStairs(n - 2);

    return arr[n]
};